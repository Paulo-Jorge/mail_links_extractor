#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import subprocess
import urllib
import lxml.html
import urlparse

def EmailExtractor(source):
    email_pattern = re.compile(r'\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b',re.IGNORECASE)
    emails = email_pattern.findall(source)
    return emails
    
def GetLinks(source_link):
    source_hostname = urlparse.urlparse(source_link).hostname
    source_scheme = urlparse.urlparse(source_link).scheme

    dom =  lxml.html.fromstring(urllib.urlopen(source_link).read())
    dom.make_links_absolute(source_scheme+'://'+source_hostname)
    links =  []
    for link in dom.xpath('//a/@href'):
        if urlparse.urlparse(link).hostname == source_hostname:
            links.append(link.lower().replace(' ','').replace('\n','').replace('\r','').replace('\t',''))
    return list(set(links)) #set removes repeated elements, list converts back to list format

main_page = raw_input("Website?")

extracted_emails = []
extracted_links = [main_page]
visited_links = []

extracted_links.extend(GetLinks(main_page))

for link in extracted_links:
    if link not in visited_links:
        try:
            page = urllib.urlopen(link).read()
            emails = EmailExtractor(page)
            print link
            print emails
            extracted_emails.extend(emails)
            visited_links.append(link)
            
            new_links = GetLinks(link)
            for l in new_links:
                if l not in extracted_links:
                    extracted_links.append(l)
        except:
            pass
    else:
        pass

#delete duplicates
extracted_emails = list(set(extracted_emails))
extracted_emails.sort()

print '================================'
print '================================'
print '================================'
print 'Emails Extracted:'
print extracted_emails
print '================================'
print '================================'
print '================================'

#from list to string divided with commas
emails_list = ", ".join(extracted_emails)
emails_list = emails_list.lower()

links_list = "\r\n".join(extracted_links)
links_list = links_list.lower()

file = open("extracted_emails.txt", "w")
file.write(emails_list)
file.close()

file = open("extracted_links.txt", "w")
file.write(links_list)
file.close()

p = subprocess.Popen(["notepad.exe", 'emails_list.txt'])
# ... do other things while notepad is running
returncode = p.wait() # wait for notepad to exit
